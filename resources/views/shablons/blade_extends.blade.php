@extends('layouts.extends')



{{-- @include('shablons.bootstrap_class', ['class' => 'btn-secondary',
'var1' => 'first', 'var2' => 'Second', 'var3' => 3000, 'style' => 'red',
'href' => 'https://laravel.ru/docs/v5/views',
'text' => 'Laravel.ru — русское сообществоLaravel по-русски']) --}}

@section('main')
    Hello, {{ $myname }} !<br>
    Тема занятий : Наследование шаблонов Blade.<br>
    <div>
        <ol>
            @foreach ($links as $item)
                <li>
                    <a href="http://{{ $item['href'] }}">{{ $item['text'] }}</a><br>
                </li>
            @endforeach
        </ol>

        {{-- {{$employees['name']}} --}}

        <div align="center">
            <br>
            <table>

                @foreach ($employees as $item)
                    <tr>
                        <td>{{ $item['name'] }} </td>
                        <td>{{ $item['surname'] }} </td>
                        <td>{{ $item['salary'] }} </td>
                    </tr>
                @endforeach
            </table>
            <br><br>

            <table>
                <tr align="center">
                    <th align="center"> id </th>
                    <th align="center"> Имя </th>
                    <th align="center"> Фамилия </th>
                    <th align="center"> Зарплата </th>

                    @foreach ($employees as $key => $item)
                <tr align="center">
                    <td align="center"> {{ $key + 1 }} </td>
                    <td align="center"> {{ $item['name'] }} </td>
                    <td align="center"> {{ $item['surname'] }} </td>
                    <td align="center"> {{ $item['salary'] }} </td>
                </tr>
                @endforeach
                </tr>
            </table>
            <br><br>

            <table>
                <tr align="center">
                    <th align="center"> id </th>
                    <th align="center"> Имя </th>
                    <th align="center"> Фамилия </th>
                    <th align="center"> Зарплата </th>

                    @foreach ($employees as $key => $item)
                        @if ($item['salary'] > 2000)
                <tr align="center">
                    <td align="center"> {{ $key + 1 }} </td>
                    <td align="center"> {{ $item['name'] }} </td>
                    <td align="center"> {{ $item['surname'] }} </td>
                    <td align="center"> {{ $item['salary'] }} </td>
                </tr>
                @endif
                @endforeach

                </tr>
            </table>
            <br><br>

            <table>
                <tr align="center">

                    <th align="center"> Имя </th>
                    <th align="center"> Фамилия </th>
                    <th align="center"> Статус </th>

                    @foreach ($users as $key => $item)
                        @if ($item['banned'] == true)
                <tr align="center">

                    <td align="center"> {{ $item['name'] }} </td>
                    <td align="center"> {{ $item['surname'] }} </td>
                    <td align="center" class="active"> активен </td>
                </tr>
            @elseif ($item['banned'] == false)
                <tr align="center">

                    <td align="center"> {{ $item['name'] }} </td>
                    <td align="center"> {{ $item['surname'] }} </td>
                    <td align="center" class="banned"> забанен </td>
                </tr>
                @endif
                @endforeach
                </tr>
            </table>
            <br><br>

            <table>
                @foreach ($lor as $item)
                    <tr align="center">
                        <td align="center"><input value="{{ $item }}"> </td>

                    </tr>
                @endforeach
            </table>
            <br><br>

            {{-- <table> --}}
            @foreach ($lor as $item)
                {{-- <tr> --}}
                {{-- <td> --}}
                <p><select>
                        <option autofocus value="{{ $item }}"></option>

                    </select></p>

                {{-- </tr> --}}
            @endforeach
            {{-- </table> --}}
            <br><br>

            <p><select>
                    <option value="{{ implode(' ', $lor) }}"></option>

                </select></p><br><br>

            <ul>
                @foreach ($rand_day as $item)
                    @if ($item == $day_today)

                        <li align="center" class="active"> {{ $item }} </li>
                    @else
                        <li align="center"> {{ $item }} </li>

                    @endif
                @endforeach
            </ul>









        </div>

        <br>
    </div>
@endsection



@section('sidebar')
    @parent
    sidebar children
@endsection

@section('title')
    Laravel 7.4.3
@endsection

@section('footer')
    @parent
@endsection
