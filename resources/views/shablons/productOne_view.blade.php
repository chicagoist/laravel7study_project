@extends('layouts.product')


@section('post_title_one')
  Продукт {{$value['name']}}
@endsection

@section('post_header_one')
  <p>Категория {{$key}}<br>
   {{$value['name']}}</p>
@endsection

@section('post_main_one')
{{-- <pre>
COUNT {{ count($value, 1) }}<br>
</pre> --}}
    <div class="info">
       <p>Цена: <span class="date"> {{ isset($value['cost']) ? $value['cost'] : null }}</span></p>
       <p>На складе: <span class="author"> {{ $value['inStock'] ? 'В наличии' : "Отсутствует" }}</span></p>
       <p>Важно: <span class="author"> {{ isset($value['desc']) ? $value['desc'] : null }}</span></p>
    </div>
@endsection


{{-- @section('post_title')
    Список страниц
@endsection

@section('post_header')
    <h1>Список страниц</h1>
@endsection

@section('post_main')

    @foreach ($posts as $key => $post)
        <div class="post">
            <h2>{{ isset($post['title']) ? $post['title'] : "Извините, страницы с :id $id не существует!" }}</h2>
            <div class="info">
                <span class="date">{{ isset($post['date']) ? $post['date'] : null }}</span>
                <span class="author">{{ isset($post['author']) ? $post['author'] : null }}</span>
            </div>
            <div class="text">
                {{ isset($post['teaser']) ? $post['teaser'] : null }}
            </div>
            <div class="more">
                <a href="/posts/{{ $key }}">ссылка на пост</a>
            </div>
        </div>

    @endforeach

@endsection --}}
