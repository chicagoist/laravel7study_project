@extends('layouts.posts')

{{-- {{ print_r($posts, 1) }} <br> --}}

{{-- @section('post_title_one')
    {{ isset($posts['title']) ? $posts['title'] : 'страницы с :id 6 не существует' }}
@endsection

@section('post_header_one')
    {{ isset($posts['title']) ? $posts['title'] : "Извините, страницы с :id $id не существует!" }}
@endsection

@section('post_main_one')

    <div class="info">
        <span class="date"> {{ isset($posts['date']) ? $posts['date'] : null }}</span>
        <span class="author"> {{ isset($posts['author']) ? $posts['author'] : null }}</span>
    </div>
    <div class="text">
        {{ isset($posts['teaser']) ? $posts['teaser'] : null }}
    </div>

@endsection --}}


@section('post_title')
    Список страниц
@endsection

@section('post_header')
    <h1>Список страниц</h1>
@endsection

@section('post_main')

    @foreach ($posts as $key => $post)
        <div class="post">
            <h2>{{ isset($post['title']) ? $post['title'] : "Извините, страницы с :id $id не существует!" }}</h2>
            <div class="info">
                <span class="date">{{ isset($post['date']) ? $post['date'] : null }}</span>
                <span class="author">{{ isset($post['author']) ? $post['author'] : null }}</span>
            </div>
            <div class="text">
                {{ isset($post['teaser']) ? $post['teaser'] : null }}
            </div>
            <div class="more">
                <a href="/posts/{{ $key }}">подробнее...</a>
            </div>
        </div>

    @endforeach

@endsection
