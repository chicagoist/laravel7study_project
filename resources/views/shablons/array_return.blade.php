<!DOCTYPE html>

<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>


    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@200;600&display=swap" rel="stylesheet">

    <!-- Styles -->
    <style>
        html,
        body {
            background-color: #fff;
            color: #636b6f;
            font-family: 'Nunito', sans-serif;
            font-weight: 200;
            height: 1000vh;
            margin: 0;
        }

        .full-height {
            height: 500vh;
        }

        .flex-center {
            align-items: center;
            display: flex;
            justify-content: center;
        }

        .position-ref {
            position: relative;
        }

        .top-right {
            position: absolute;
            right: 10px;
            top: 18px;
        }

        .content {
            text-align: center;
        }

        .title {
            font-size: 84px;
        }

        .links > a {
            color: #636b6f;
            padding: 0 25px;
            font-size: 13px;
            font-weight: 600;
            letter-spacing: .1rem;
            text-decoration: none;
            text-transform: uppercase;
        }

        .m-b-md {
            margin-bottom: 30px;
        }

        .nav-masthead {
            padding: .25rem 0;
            font-weight: 700;
            color: rgba(160, 2, 2, 0.5);
            background-color: transparent;
            border-bottom: .25rem solid transparent;
        }

        .nav-masthead,
        .nav-masthead {
            border-bottom-color: rgba(255, 255, 255, .25);
        }

        .nav-masthead {
            margin-left: 1rem;
        }

        .nav-masthead {
            color: #fff;
            border-bottom-color: #fff;
        }

        .btn-secondary,
        .btn-secondary:hover,
        .btn-secondary:focus {
            color: #333;
            text-shadow: none;
            /* Prevent inheritance from `body` */
        }

        .colortext_black {
            color: black;
            font-weight: bold;
        }

        .colortext_italic {
            color: green;
            font-style: italic;
        }

        .colortext {
            color: {!! $style !!};
            font-weight: bold;
        }

        .letter {
            color: blueviolet;
            font-size: 30px;
            text-decoration: dashed;

        }

    </style>
</head>

<body>
{{ $h1 }}
<div>

    <div class="flex-center position-ref full-height">
        <div class="content">
            <div class="content position-ref">
                <p><span class="colortext">

                            Количество элементов массива : {{ count($arr) }}<br>
                <p span class="colortext">Имя из массива :
                    {{ isset($arr['name']) ? $arr['name'] : 'Default' }}
                </p>
                <p span class="colortext">Возраст из массива : {{ $arr['age'] }}</p>
                <p span class="colortext">Зарплата из массива : {{ $arr['salary'] }}</p>
                <p span class="colortext">Проверка на существование : {{ isset($name) ? $name : 'Default' }}
                </p>
                <p span class="colortext">Имя из массива : {{ isset($stylee) ? $style : 'Default' }}</p>
                @php
                    echo '
                    <pre>';
                 echo print_r($arr, 1);
             echo '</pre>';
                @endphp

                Hello, @{{ $style }}.<br>
                City name : {{ isset($city) ? $city : 'Москва' }}<br>

                <ul>
                    @foreach ($location as $key => $item)
                        <li>{{ isset($item['country']) ? $item['country'] : 'Россия' }} --
                            {{ isset($item['city']) ? $item['city'] : 'Москва' }}
                        </li>
                    @endforeach
                    {{-- @foreach ($location as $key => $item)
                        {{ $item['country'] or 'Россия' }} --
                        {{ $item['city'] or 'Москва' }}<br>
                    @endforeach --}}
                </ul>


                Год : {{ isset($year) ? $year : date('Y') }}<br>
                Месяц : {{ isset($month) ? $month : date('m') }}<br>
                День : {{ isset($day) ? $day : date('d') }}<br>
                Неэкранированые данные : {!! $str !!}
                @php
                    echo 'Неэкранированые данные :' . $str . '<br>';
                @endphp

                @if ($weekDay == 7)
                    <p span class="letter">Наверное сегодня Воскресенье</p>
                @elseif ($weekDay == 6)
                    <p span class="letter">Наверное сегодня Суббота</p>
                @else
                    <p span class="letter">Наверное сегодня будний день</p>

                @endif

                @unless($isAuth ?? '')
                    вы еще не авторизованы<br>
                @endunless


                @if (count($arrNum) != 0)
                    @php
                        $num = 0;

                        for ($i = 0; $i < count($arrNum); $i++){ $num=$num + $arrNum[$i]; }
                            echo "Сумма элементов массива" . "<i>" . $num . "</i>" . "<br>" ; @endphp
                @elseif(!count($arrNum)) Количество элементов массива равно нулю.<br>
                @endif

                @foreach ($arrNum as $item)
                    <p>{{ $item }}</p>
                @endforeach

                Элементы массива в квадрате:<br>
                <ul>
                    @foreach ($arrNum as $item)
                        <li>{{ $item ** 2 }}</li>
                    @endforeach
                </ul>

                Элементы массива в квадратные корни:<br>
                <ul>
                    @foreach ($arrNum as $item)
                        <li>{{ sqrt($item) }}</li>
                    @endforeach
                </ul>
                <br>


                @foreach ($arr as $key => $elem)
                    <ul>{{ $key }} {{ $elem }}</ul>
                @endforeach<br>


                @foreach ($arrNum as $key => $elem)
                    Ключ: {{ $key + 1 }} Значение: {{ $elem }}<br>
                @endforeach<br>

                <ul>
                    @foreach ($arrNum as $item)
                        @if ($item % 2 == 0)
                            <li> {{ $item }}</li>
                        @endif

                    @endforeach
                </ul>
                <br>


                @if (is_array($data))
                    <ul>
                        @foreach ($data as $key => $elem)
                            <li>Значение: {{ $elem }}</li>
                        @endforeach
                    </ul><br>

                @elseif (is_int($data))
                    <p>Не массив, а число: {{ $data }}</p>

                @endif
                <br>

                {{-- <table>
                    @foreach ($users as $subArr)
                        <tr>
                            @foreach ($subArr as $elem)
                                <td>{{ $elem }}</td>
                            @endforeach
                        </tr>
                    @endforeach
                </table> --}}

                <table style="margin: 0px auto;">


                    @for ($i = 0; $i < count($users); $i += 5)
                        <tr>
                            @for ($a = 0; $a < 5; $a++)
                                <td>{{ $users[$i] + $a }}</td>

                            @endfor
                        </tr>
                    @endfor


                </table>

                <table>
                    @php
                        for($i = 0; $i < count($users); $i +=5){
                            for ($a=0; $a < 5; $a++) {
                                echo $users[$i]+$a . " " ; } echo "<br>";
                            }
                    @endphp
                </table>

                <p>
                <ul>
                    @foreach ($users_mas as $user)
                        <li> {{ $user['name'] }} {{ $user['surname'] }} {{ $user['salary'] }}</li>
                    @endforeach
                </ul>
                </p>

                <table style="margin: 0px auto;">

                    @foreach ($users_mas as $user)
                        <tr>
                            <td> {{ $user['name'] }} {{ $user['surname'] }} {{ $user['salary'] }} </td>
                        </tr>
                    @endforeach

                </table>


                <ul>
                    @foreach ($users_mas as $user)
                        @if ($loop->first)
                            <li span class="letter">Это пользователь {{ $user['name'] }}</li>
                        @endif

                        @if ($loop->last)
                            <li span class="colortext">Это пользователь {{ $user['name'] }}</li>
                        @endif
                    @endforeach
                </ul>

                {{-- <table>

                    @for($i = 0; $i < count($users); $i +=5)
                     <tr>
                          @for ($a=0; $a < 5; $a++)
                           @if ($loop->iteration == 3)
                             <td span class="colortext_italic"> {{$users[$i]+$a}}</td>
                             @else
                              <td span class="colortext_black"> {{$users[$i]+$a}}</td>
                           @endif
                          @endfor
                     </tr>
                    @endfor
               </table> --}}

                <ul>
                    @foreach ($users as $key => $user)

                        @if($loop->remaining < 3)
                            <li span class="colortext_italic"> {{$user}}</li>
                        @else
                            <li span class="colortext_black"> {{$user}}</li>
                        @endif

                    @endforeach
                </ul>

                @forelse($users_mas as $elem)
                    <p>{{ implode(" ", $elem) }}</p>
                @empty
                    <p>В массиве нет элементов</p>
                @endforelse

                @for ($i = 0; $i < 100; $i += 10)
                    <p>
                        @for ($a = 0; $a < 10; $a++)
                            {{$i + $a}}

                        @endfor
                    </p>

                @endfor


            </div>


            <div class="links">
                <a href="https://laravel.com/docs">Docs</a>
                <a href="https://laracasts.com">Laracasts</a>
                <a href="https://laravel-news.com">News</a>
                <a href="https://blog.laravel.com">Blog</a>
                <a href="https://nova.laravel.com">Nova</a>
                <a href="https://forge.laravel.com">Forge</a>
                <a href="https://vapor.laravel.com">Vapor</a>
                <a href="https://github.com/laravel/laravel">GitHub</a>
            </div>
        </div>
    </div>

</body>

</html>
