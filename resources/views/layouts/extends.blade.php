<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    @include('elems.header')
</head>

<body>

    <div class="content">
        <div class="title m-b-md">
            Blade Extends
        </div>
    </div>
    <div class="colortext_black">



        <div>

            <h3>
                <main>
                    @yield('main')<br>
                </main>

            </h3>

        </div>






    </div>

    <div>
        @include('elems.footer')

    </div>

</body>

</html>


{{-- @section('site')
<!DOCTYPE html>
<html>

<head>
    <title>Тайтл страницы @yield('title')</title>
</head>

<body>
    <header>
        хедер
    </header>
    @section('sidebar')
    <aside>
        сайдбар
    </aside>


    <main>
        @yield('main')
    </main>
    <footer>
        футер
    </footer>
</body>

</html>
@endsection --}}
