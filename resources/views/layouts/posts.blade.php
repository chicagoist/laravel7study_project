<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
		<title>
            @yield('post_title_one')
            @yield('post_title')
        </title>
	</head>
	<body>
		<header>
			<h2>@yield('post_header_one')</h2>
			<h2>@yield('post_header')</h2>
		</header>
		<main>
             @yield('post_main_one')
             @yield('post_main')
		</main>
	</body>
</html>
