<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">

<title>Laravel - @yield('title')</title>

<!-- Fonts -->
<link href="https://fonts.googleapis.com/css2?family=Nunito:wght@200;600&display=swap" rel="stylesheet">

<!-- Styles -->
<style>
    html,
    body {
        background-color: #fff;
        color: #636b6f;
        font-family: 'Nunito', sans-serif;
        font-weight: 200;
        height: 500vh;
        margin: 0;
    }

    .full-height {
        height: 500vh;
    }

    .flex-center {
        align-items: center;
        display: flex;
        justify-content: center;
    }

    .position-ref {
        position: relative;
    }

    .top-right {
        position: absolute;
        right: 10px;
        top: 18px;
    }

    .content {
        text-align: center;
    }

    .title {
        font-size: 84px;
    }

    .links>a {
        color: #636b6f;
        padding: 0 25px;
        font-size: 13px;
        font-weight: 600;
        letter-spacing: .1rem;
        text-decoration: none;
        text-transform: uppercase;
    }

    .m-b-md {
        margin-bottom: 30px;
    }

    .colortext_black {
        color: black;
        font-weight: bold;
        text-align: center;

    }

    .colortext_italic {
        color: green;
        font-style: italic;
        text-align: center;
    }

    .colortext {
        color: #636b6f;
        font-weight: bold;
        text-align: center;
    }

    .letter {
        color: blueviolet;
        font-size: 30px;
        text-decoration: dashed;

    }
    .banned {
        color: red;


    }
    .active {
        color: green;


    }

</style>
