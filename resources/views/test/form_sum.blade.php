<!DOCTYPE html>
<html lang="ru">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>

<body>

    <p>Сумма чисел равна: {{ $sum }}<br></p>

    <p>
        Все данные из формы со слагаемыми:

        @php
            var_dump($data);
        @endphp

        <br>
    <ul>
        @foreach ($data as $item)
            @if (is_array($item))
                @foreach ($item as $value)
                    <li>
                        {{ $value }}
                    </li>
                @endforeach
            @endif
            @if (!is_array($item))

                    <li>
                        {{ $item }}
                    </li>

            @endif
        @endforeach
    </ul>


    </p>

</body>

</html>
