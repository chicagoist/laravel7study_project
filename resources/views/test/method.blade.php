@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">


                @php
                    echo ' Для указанного адреса выведите результат метода path. :  ';
                    echo '<b>', Request::path(), '</b>', '<br>';

                    echo 'Для указанного адреса выведите результат метода url. :  ';
                    echo '<b>', Request::url(), '</b>', '<br>';

                    echo 'Для указанного адреса выведите результат метода fullUrl. :  ';
                    echo '<b>', Request::fullUrl(), '</b>', '<br>';

                    echo "С помощью метода fullUrlWithQuery добавьте к запрошенному URL GET";
                    printf("<br>");
                    echo "параметр page со значением 1. :  ";
                    echo '<b>', Request::fullUrlWithQuery(['page' => 1]), '</b>' , '<br>';

                    echo "Самостоятельно попробуйте поработать с методом is : ";

                    //echo Request::is('item/method');

                    if ($request->is('item/*')) {
                        echo "<b>" , '$request->is(\'item/*\'))' , "</b>";
                    }

                @endphp


            </div>
        </div>
    </div>
    </div>
@endsection
