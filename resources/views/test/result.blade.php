<!DOCTYPE html>
<html lang="ru">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>

<body>

    <ul>
        @if (isset($data) && is_array($data))
            @foreach ($data as $key => $elem)
                @if ($key != '_token')
                    <li>{{ $key }}: {{ $elem }}</li>
                @endif

            @endforeach
        @endif

        @if (isset($param))
            <li>item route: {{ $param }}</li>
        @endif
    </ul>

</body>

</html>
