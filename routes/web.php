<?php

use App\Http\Controllers\Controller;
use App\Http\Controllers\ExtendsController;
use App\Http\Controllers\ProductController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::get('/', function () {
    return view('welcome');
});



Route::get('/test/', function () {
    return "!";
});

Route::get('/dir/test', function () {
    return "!!";
});


Route::get('sum/{id1}/{id2}', function ($id1, $id2) {
    return 'Sum = '.(int)($id1 + $id2);
});

Route::get('user/{id?}', function ($id = 0) {
    return  'Параметр ' . $id;
})->where('id', '[0-9]');

Route::get('user/{id?}/{name?}', function ($id = 1, $name = 'John') {
    echo  'Номер и Имя:  ' . $id;
    echo ' ' . $name;
})->where(['id' => '[0-9]+', 'name' => '[a-z]{2}']);

Route::get('articles/{date}', function ($date) {
    echo  'Дата в формате год-месяц-день: ' . $date;
})->where('date', '[0-9]{4}-[0-1]{1}[0-9]{1}-[0-3]{1}[0-9]{1}');

Route::get('users/{order}', function ($order) {
    return  'Order: ' . $order;
})->where('order', '(name|surname|age)');
//-([0]{1}[1-9]{1}|[1]{1}[0-9]{1}|[2]{1}[0-9]{1}|[3]{1}[0-1]{1})

Route::get('{year}/{month}/{day}', function ($year, $month, $day) {
    $day_of_week = ($year.$month.$day);
    return 'День недели ' . date("w", strtotime($day_of_week));
})->where('year', '[(19|20)]{2}[0-9]{2}')->where('month', '[0-1]{1}[0-9]{1}')->
where('day', '[0-3]{1}[0-9]{1}');


// Route::get('test/show/{id?}', function ($id = 'TestController') {
//  $a = new App\Http\Controllers\TestController();
//  return $a->show($id);
// })->where('id', '[0-9]+');
Route::get('test/show/{id?}', 'TestController@show')->where('id', '[0-9]+');




Route::get('pages/show/{id?}', function ($id = 'Pages') {
    $a = new App\Http\Controllers\Page();
    return $a->showOne($id);
})->where('id', '[0-9]+');



Route::get('pages/all', 'Page@showAll');



Route::get('actions', function () {
    $url = action('TestController@show');

    return var_export($url);
});


Route::get('test/show/{param1}/{param2}', 'TestControllerSome@show');
Route::get('test/sum/{param1}/{param2}', 'Test@sum')->where('param1', '[0-9]+')->
where('param2', '[0-9]+');

Route::get('employee/{param1}', 'Employee@showOne');

Route::get('employee/{num}/{string}', 'Employee@showField')->where('num', '[0-9]+')->
where('string', '[a-zA-Z]+');

Route::get('test/view', 'Test1Controller@show');
Route::get('test/view/{id}', 'TestController@show')->where('id', '[0-9]+');

Route::get('test/show/{param}', 'TestController@show');

// Route::resource('photos', 'PhotoController');
// Route::resources([
//     'photos' => 'PhotoController',
//     'posts' => PostController::class,
// ]);

Route::get('employee/{name}/{age}/{salary}', 'ShablonController')->where('name', '[A-Za-z]+')->
where('age', '[0-9]+')->where('sallary', '[0-9]+');

Route::get('test/bootstrap', 'BootstrapController');

Route::get('array/{name?}/{age?}/{salary?}', 'ArrayController@show')->where('name', '[A-Za-z]+')->
where('age', '[0-9]+')->where('salary', '[0-9]+');

Route::get('blade/extends', 'ExtendsController@show');

// Route::get('practice/101', 'PostController@show');
Route::get('posts/{id}', 'PostController@showOne')->name('postOne');
Route::get('posts', 'PostController@showAll');

Route::get('product/{category_id}/{product_id}', 'ProductController@showProduct')->
where('category_id', '[0-9]+')->where('product_id', '[0-9]+')->name('productOne');

Route::get('product/{category_id}/', 'ProductController@showCategory')->
where('category_id', '[0-9]+');

Route::get('categories', 'ProductController@showCategoryList');

Route::match(['get', 'post'], '/test/form', 'TestController@form');
//Route::post('/test/form/sum', 'TestController@form_sum');

Route::any('/item/{id}', 'UserController@form')->where('id', '[0-9]+');
Route::any('/item/{method}', 'UserController@method');
Route::any('/item/form/{id}', 'UserController@update');


Route::any('/session/id', 'TestSessionController@show');
Route::any('/session/get', 'TestSessionController@getSession');
Route::any('/session/check', 'TestSessionController@getCheck');
Route::any('/session/flush', 'TestSessionController@flush');
Route::any('/session/time', 'TestSessionController@getFirstTime');
Route::any('/session/arr/{param}', 'TestSessionController@pushArr')->where('param', '[A-Za-z0-9]+');








Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

// Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');
