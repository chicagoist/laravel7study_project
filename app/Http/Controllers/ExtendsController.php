<?php

namespace App\Http\Controllers;


class ExtendsController extends Controller
{
    private $links = [];
    private $users = [];
    private $employees = [];
    private $lor = [];
    private $lor_string = 'Lorem ipsum dolor sit, amet consectetur adipisicing elit. Consequatur deserunt illum necessitatibus, eum, est cupiditate fugit dolore amet nesciunt velit, atque beatae error debitis culpa ratione illo possimus ipsum consequuntur?';
    private $rand_day = [];
    private $day_today;




    public function __construct()
    {
        $this->links = [
            [
                'text' => 'text1',
                'href' => 'href1',
            ],
            [
                'text' => 'text2',
                'href' => 'href2',
            ],
            [
                'text' => 'text3',
                'href' => 'href3',
            ],
        ];

        $this->employees = [
            [
                'name' => 'user1',
                'surname' => 'surname1',
                'salary' => 1000,
            ],
            [
                'name' => 'user2',
                'surname' => 'surname2',
                'salary' => 2000,
            ],
            [
                'name' => 'user3',
                'surname' => 'surname3',
                'salary' => 3000,
            ],
            [
                'name' => 'user4',
                'surname' => 'surname4',
                'salary' => 4000,
            ],
            [
                'name' => 'user5',
                'surname' => 'surname5',
                'salary' => 5000,
            ],
        ];

        $this->users = [
            [
                'name' => 'user1',
                'surname' => 'surname1',
                'banned' => true,
            ],
            [
                'name' => 'user2',
                'surname' => 'surname2',
                'banned' => false,
            ],
            [
                'name' => 'user3',
                'surname' => 'surname3',
                'banned' => true,
            ],
            [
                'name' => 'user4',
                'surname' => 'surname4',
                'banned' => false,
            ],
            [
                'name' => 'user5',
                'surname' => 'surname5',
                'banned' => false,
            ],
        ];



    }


    public function show()
    {
        $this->day_today = date("j");

        for ($i=0; $i < date('t', time()); $i++) {
            $this->rand_day[$i] = rand(1, date('t', time()));
        }

        $this->myname = 'Валерий';
        return  view('shablons.blade_extends', [
            'links' => $this->links,
            'myname' => $this->myname,
            'employees' => $this->employees,
            'users' => $this->users,
            'lor' => $this->lor,
            'rand_day' => $this->rand_day,
            'day_today' => $this->day_today
        ]);
    }
}
