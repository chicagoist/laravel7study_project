<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

class ArrayController extends Controller
{
    private $location = [];
    private $arrNum = [];
    private $rand_int = null;
    private $data = null;
    private $users = [];
    private $users_mass = [];

    public function __construct()
    {
        $this->location = [
            [
                'country' => null,
                'city' => null

            ],
            [
                'country' => 'Германия',
                'city' => 'Оффенбах'

            ],
            [
                'country' => 'Украина',
                'city' => 'Кременчуг'

            ],
            [
                'country' => 'Турция',
                'city' => 'Измир'

            ],
            [
                'country' => 'Франция',
                'city' => 'Гренобль'

            ],
        ];

        $this->arrNum = [1, 2, 3, 4, 5];

        $this->rand_int = random_int(1, 2);

        $this->users =
            [
                1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15,
                16, 17, 18, 19, 20, 21, 22, 23, 24, 25
            ];


        $this->users_mass = [
            [
                'name' => 'user1',
                'surname' => 'surname1',
                'salary' => 1000,
            ],
            [
                'name' => 'user2',
                'surname' => 'surname2',
                'salary' => 2000,
            ],
            [
                'name' => 'user3',
                'surname' => 'surname3',
                'salary' => 3000,
            ],
        ];
    }


    public function show(String $country = null, Int  $age = null, Int $salary = null)
    {
        $arr = ['country' => $country, 'age' => $age, 'salary' => $salary];

        if ($this->rand_int == 1) {
            $this->data = range(1, 8);
            shuffle($this->data);
        } elseif ($this->rand_int == 2) {
            $this->data = random_int(1, 10);
        }


        return  view('shablons.array_return', [
            'arr' => ['country' => $country, 'age' => $age, 'salary' => $salary],
            'style' => 'red', 'h1' => '<h1>', 'city' => null, 'location' => $this->location,
            'year' => null, 'month' => null, 'day' => null, 'str' => "<h3>строка</h3>", 'weekDay' => date('w'),
            'isAuth' => false, 'arrNum' => $this->arrNum, 'data' => $this->data, 'users' => $this->users,
            'users_mas' => $this->users_mass
        ]);

        // return  view('shablons.array_return', ['country' => $country,
        // 'age' => $age, 'salary' => $salary]);



    }
}
