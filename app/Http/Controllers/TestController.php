<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class TestController extends Controller
{
    public function show($param = null)
    {
        //$this->param = $param;
        return 'Квадрат числа id = ' . $param ** 2;
    }

    public function form(Request $request, $user, $age, $login)
    {
        // Здесь доступна переменная $request
        //if ($request->has('number')) {
        // var_dump($request->input('text'));
        var_dump($request->input('number') ** 2);
        echo 'method :';
        var_dump($request->method());

        $data = $request->only('user', 'age');
        //$data = $request->except(['login', 'password']);
        //$data = $request->all();
        //var_dump($data);



        if ($request->isMethod('post')) {
            $sum = $request->input('number1') + $request->input('number2');

            return view('test.form_sum', [
                'number1' => $request->input('number1'),
                'number2' => $request->input('number2'),
                'number' => $request->input('number') ** 2,
                'sum' => $sum,
                'data' => $data
            ]);
        }

        if ($request->isMethod('get')) {
            return view('test.form', [
                'quart' => $request->input('number') ** 2,
                'number' => $request->input('number'),

            ]);
        }
    }

    public function form_sum(Request $request)
    {
        echo 'method :';
        var_dump($request->method());

        if ($request->isMethod('post')) {
            echo "!!<br>";
        } else {
            echo "!<br>";
        }


        $sum = $request->input('number1') + $request->input('number2');

        return view('test.form_sum', [
            'number1' => $request->input('number1'),
            'number2' => $request->input('number2'),
            'number' => $request->input('number') ** 2,
            'sum' => $sum
        ]);
    }
}
