<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

class ProductController  extends Controller
{
    private $categories;

    public function __construct()
    {
        $this->categories = [
            1 => [
                'name' => 'Категория 1',
                'products' => [
                    1 => [
                        'name'    => 'Продукт 1',
                        'cost'    => '300',
                        'inStock' => true,
                        'desc'    => 'Описание продукта 1'
                    ],
                    2 => [
                        'name'    => 'Продукт 2',
                        'cost'    => '400',
                        'inStock' => true,
                        'desc'    => 'Описание продукта 2'
                    ],
                    3 => [
                        'name'    => 'Продукт 3',
                        'cost'    => '500',
                        'inStock' => false,
                        'desc'    => 'Описание продукта 3'
                    ],
                ],
            ],
            2 => [
                'name' => 'Категория 2',
                'products' => [
                    1 => [
                        'name'    => 'Продукт 1',
                        'cost'    => '700',
                        'inStock' => true,
                        'desc'    => 'Описание продукта 1'
                    ],
                    2 => [
                        'name'    => 'Продукт 2',
                        'cost'    => '800',
                        'inStock' => false,
                        'desc'    => 'Описание продукта 2'
                    ],
                    3 => [
                        'name'    => 'Продукт 3',
                        'cost'    => '900',
                        'inStock' => false,
                        'desc'    => 'Описание продукта 3'
                    ],
                    4 => [
                        'name'    => 'Продукт 4',
                        'cost'    => '1000',
                        'inStock' => true,
                        'desc'    => 'Описание продукта 4'
                    ],
                ],
            ],
        ];
    }

    public function showProduct(Int $category_id, Int $product_id)
    {
        $count_id = count($this->categories[$category_id]['products']);

        foreach ($this->categories as $key => $value) {
             {
                if ($key == $category_id) {
                    foreach ($value as $key_i => $item) {
                        if($product_id <= $count_id) {

                            return view('shablons.productOne_view', ['value' => $value['products'][$product_id],
                            'key' => $key]);

                        }else {


                            // return view('shablons.productOne_view', ['value' => $value['products'][$product_id],
                            // 'key' => $key]);
                        }

                    }

                }
            }
        }
    }

    public function showCategory (Int $category_id) {

        $count_id = count($this->categories);

        if($category_id <= $count_id) {

            // echo $count_id . "<br>";
            // echo "<pre>";
            // print_r($this->categories[$category_id]['products']);
            // echo "</pre>";


            return view('shablons.categoryOne_view', ['value' => $this->categories[$category_id]['products'],
                          'count_id' => $count_id, 'category_id' => $category_id, 'product_id' => 2,
                          'categories' => $this->categories]);

        }



        // foreach ($this->categories as $key => $value) {
        //      {
        //         if ($key == $category_id) {
        //             foreach ($value as $key_i => $item) {
        //                 if($product_id <= $count_id) {

        //                     return view('shablons.productOne_view', ['value' => $value['products'][$product_id],
        //                     'key' => $key]);

        //                 }else {


        //                     // return view('shablons.productOne_view', ['value' => $value['products'][$product_id],
        //                     // 'key' => $key]);
        //                 }

        //             }

        //         }
        //     }
        // }
    }

    public function showCategoryList() {

        $count_id = count($this->categories);



        return view('shablons.listOfCategories_view', ['count_id' => $count_id,
        'categories' => $this->categories]);

    }



}
