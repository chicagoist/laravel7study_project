<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

class Employee extends Controller
{
    private $employees;
    private $worker = [];
    private $fieldWorker = null;
    private $fielsKey = null;

    public function __construct()
    {
        $this->employees = [
            1 => [
                'name' => 'user1',
                'surname' => 'surname1',
                'salary' => 1000,
            ],
            2 => [
                'name' => 'user2',
                'surname' => 'surname2',
                'salary' => 2000,
            ],
            3 => [
                'name' => 'user3',
                'surname' => 'surname3',
                'salary' => 3000,
            ],
            4 => [
                'name' => 'user4',
                'surname' => 'surname4',
                'salary' => 4000,
            ],
            5 => [
                'name' => 'user5',
                'surname' => 'surname5',
                'salary' => 5000,
            ],
        ];
    }

    public function showOne($param = null)
    {

        foreach ($this->employees as $key => $value) {
            if ($param == $key) {
                $worker = $value;
            }
        }
        //return implode(" ", $worker);

        //return view('employee', ['var1' => implode(" ", $worker)]);
        return view('test_employee.employee', ['worker' => implode(" ", $worker)]);
    }

    public function showField($num = null, $str_param = null)
    {

        foreach ($this->employees as $key => $value) {
            if ($num == $key) {
                $this->fielsKey = $key;
                foreach ($value as $id => $field) {
                    if ($id == $str_param) {
                        $this->fieldWorker = $field;
                    }
                }
            }
        }
        if ($this->fielsKey) {
            //return 'Рабочий ' . $this->fielsKey . " " . $this->fieldWorker;
            return view('employee_fields.show_fields', ['id_of_worker' => $this->fielsKey, 'str_param'=>
            $this->fieldWorker]);

        }
    }
}
