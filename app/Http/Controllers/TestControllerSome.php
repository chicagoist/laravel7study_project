<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

class TestControllerSome extends Controller
{
    public function show($param1, $param2) // задаем параметры
    {
        return $param1 . ' ' . $param2;
    }
}
