<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

class ShablonController extends Controller
{
    public function __invoke(String $name, Int $age, Int $sallary)
    {

        return view('shablons.shablon_first', ['name' => $name, 'age' => $age, 'sallary' => $sallary]);
    }

}
