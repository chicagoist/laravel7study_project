<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

class BootstrapController extends Controller
{
    public function __invoke()
    {
        //$class = $_SERVER['DOCUMENT_ROOT'] . '/public/bootstrap_examples/bootstrap-5.0.0-beta1-examples/cover/cover.css';
        //$class = '/public/bootstrap_examples/bootstrap-5.0.0-beta1-examples/cover/cover.css';
        $href = 'https://laravel.ru/docs/v5/views';
        $text = 'Laravel.ru — русское сообществоLaravel по-русски';

        return  view('shablons.bootstrap_class', ['class' => 'btn-secondary',
        'var1' => 'first', 'var2' => 'Second', 'var3' => 3000, 'style' => 'red', 'href' => $href,
        'text' => $text]);


}
}
