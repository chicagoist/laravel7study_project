<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class UserController extends Controller
{
    public function form(Request $request, $id)
    {
        var_dump($id);
        return view('test.form_route', ['id' => $id]);
    }

    public function update(Request $request, $param)
    {
        $data = $request->all();
        return view('test.result', ['param' => $param, 'data' => $data]);
    }


    public function method(Request $request, $id)
    {
        return view('test.method', ['id' => $id, 'request' => $request]);
    }
}
