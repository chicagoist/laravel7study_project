<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

class Page extends Controller
{

    public function showOne($param = null)
    {
        $id = null;
        $pages = [
            1 => 'страница 1',
            2 => 'страница 2',
            3 => 'страница 3',
            4 => 'страница 4',
            5 => 'страница 5',
        ];

        foreach ($pages as $key => $value) {
            if ($key == $param) {
                $id = $value;
            }
        }

        if($id){
            return $id;
        }else {
            return 'Нет страницы с таким id...' .$param;
        }



        //return 'Квадрат числа id = ' . $param ** 2;
    }
    public function showAll()
    {
        echo "<pre>";
        echo var_dump($this);
        echo "</pre>";
        return var_export($this);
    }
}
