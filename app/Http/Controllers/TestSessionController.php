<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;

class TestSessionController extends Controller
{
    private $valerii = 0;

    public function __construct(Request $request)
    {
        Session::put('arr', ['a', 'b', 'c']);
        //
    }

    public function show(Request $request) // выполняем инъекцию
    {
        $request->session()->put('key', 'valerii');
        $request->session()->put('arr', ['a', 'b', 'c']);

        $item = $value = $request->session();
        var_dump($item);
    }

    public function pushArr(Request $request, $id) // выполняем инъекцию
    {
        $request->session()->put('param', (int)$id);
        $arr = $request->session()->get('arr');
        var_dump($arr);
        echo "<br>";
        var_dump($value = $request->session());
        echo "<br>";
        echo $id;



        array_push($arr, $id);
        var_dump($arr);
        echo "<br>";

        Session::forget('param');
        echo "<br>";
        echo $id;

        echo "<br>";
        var_dump($value = $request->session());
        echo "<br>";
    }

    public function getSession(Request $request) // выполняем инъекцию
    {
        $value = $request->session()->get('key');
        var_dump($value);
    }

    public function getCheck(Request $request) // выполняем инъекцию
    {
        $value = $request->session()->increment('check');
        echo $value;
    }

    public function getFirstTime(Request $request) // выполняем инъекцию
    {
        if (null == $request->session()->get('timeStart')) {
            $request->session()->put('timeStart', date('D, d M Y H:i:s'));
        }

        $value = $request->session()->get('timeStart');
        //$value = date('D, d M Y H:i:s');
        echo $value;

        $item = $value = $request->session();
        var_dump($item);
    }


    public function flush(Request $request) // выполняем инъекцию
    {
        $request->session()->flush();
        $request->session()->regenerate();
    }
}
